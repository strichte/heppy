import numpy as np
import ROOT

def main():
	'''
	Generate ROOT file as input for tests.
	'''

	f = ROOT.TFile('example.root', 'recreate');

	# 1D histogram
	hist1d = ROOT.TH1F('root_histogram_1d_name', 'root_histogram_1d_title', 4, -1.0, 1.0)
	hist1d.SetBinContent(1, 1.1)
	hist1d.SetBinContent(2, 4.3)
	hist1d.SetBinContent(3, 9.5)
	hist1d.SetBinContent(4, 16.7)
	hist1d.SetBinError(1, 0.2)
	hist1d.SetBinError(2, 0.6)
	hist1d.SetBinError(3, 1.0)
	hist1d.SetBinError(4, 1.4)
	hist1d.Write('root_histogram_1d_name');

	# 2D histogram
	hist2d = ROOT.TH2F('root_histogram_2d_name', 'root_histogram_2d_title', 4, -1.0, 1.0, 2, -1.0, 1.0)
	# The zero padding is for the under-/overflows
	areas = np.transpose(np.array([
		[0., 0., 0., 0., 0., 0.],
		[0., 1., 2., 3., 4., 0.],
		[0., 5., 6., 7., 8., 0.],
		[0., 0., 0., 0., 0., 0.],
		]))
	area_errors = np.transpose(np.array([
		[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		[0.0, 0.4, 0.3, 0.2, 0.1, 0.0],
		[0.0, 0.8, 0.7, 0.6, 0.5, 0.0],
		[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		]))
	hist2d.SetBinContent(1, 1, 1.0)
	hist2d.SetBinContent(2, 1, 2.0)
	hist2d.SetBinContent(3, 1, 3.0)
	hist2d.SetBinContent(4, 1, 4.0)
	hist2d.SetBinContent(1, 2, 5.0)
	hist2d.SetBinContent(2, 2, 6.0)
	hist2d.SetBinContent(3, 2, 7.0)
	hist2d.SetBinContent(4, 2, 8.0)
	hist2d.SetBinError(1, 1, 0.4)
	hist2d.SetBinError(2, 1, 0.3)
	hist2d.SetBinError(3, 1, 0.2)
	hist2d.SetBinError(4, 1, 0.1)
	hist2d.SetBinError(1, 2, 0.8)
	hist2d.SetBinError(2, 2, 0.7)
	hist2d.SetBinError(3, 2, 0.6)
	hist2d.SetBinError(4, 2, 0.5)
	hist2d.Write('root_histogram_2d_name');


	f.Close()



if __name__ == '__main__':
	main()
