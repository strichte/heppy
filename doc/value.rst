Value with uncertainties
========================

.. automodule value
..  :members:

.. autoclass:: heppy.Value
    :members:
