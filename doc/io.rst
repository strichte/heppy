Reading and writing in different formats
========================================

Heppy
-----

Heppy histograms can be written to and read from the native text-based format
using :py:func:`heppy.basehistogram.to_file` and :py:func:`heppy.from_file`.

ROOT
----

Reading from a ROOT file:

.. autofunction:: heppy.readroot

Converting to ROOT histograms and writing to ROOT files can be done using
:py:func:`heppy.histogram1d.to_root` and
:py:func:`heppy.histogram1d.to_root_file` (for 1D histograms) and
:py:func:`heppy.histogram2d.to_root` and
:py:func:`heppy.histogram2d.to_root_file` (for 2D histograms).

Rivet (YODA)
------------

One-dimensional histograms can be written to the Rivet YODA format using
:py:func:`heppy.histogram1d.to_yoda` or its synonym
:py:func:`heppy.histogram1d.to_rivet`.

TRExFitter (YAML)
-----------------

Reading TRExFitter YAML format:

.. autofunction:: heppy.readtrex

ATLAS Isolation and Fake Forum (XML)
------------------------------------

Values with uncertainties can be converted to an XML string that the ATLAS
Isolation and Fake Forum fake background tool understands in its configuration
file using :py:func`heppy.Value.to_atlasiff`. You can use this for writing out
e.g. fake factors.
