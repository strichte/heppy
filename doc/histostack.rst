Histogram stack
===============

A collection of histograms. The total summed histogram and its variations may be accessed easily, and the stack may be plotted conveniently.

.. autoclass:: heppy.histostack
	:members:
