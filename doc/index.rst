.. Heppy documentation master file, created by
   sphinx-quickstart on Tue May  7 14:22:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Heppy: Pythonic data structures and tools for high energy physics
=================================================================

The Heppy package provides useful data structures and tools for high energy physics.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   histogram
   io
   histostack
   value
   plot
   uncertainty


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
